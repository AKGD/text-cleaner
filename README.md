# Text Cleaner

### [DEMO](https://akgd.gitlab.io/text-cleaner)

This tool can:
* Remove white space from the string (trim, remove double spaces)
* Encode strings
* Decode strings
* Swap single quotes with double or vice versa